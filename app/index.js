var React = require('react');
var ReactDom = require('react-dom');
require('./index.css');
var App = require('./components/App');


ReactDom.render(
	<App />,
	document.getElementById('app')
);

// var num = [1,2,3];
// var num10 = num.map( function(each)
// 		return each+10
// 	});
// console.log(num10);