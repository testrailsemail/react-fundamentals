var React = require('react');
var PropTypes = require('prop-types');
var Popular = require('./Popular');
var ReactRouter = require('react-router-dom');
var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;
var Nav = require('./Nav');
var Home = require('./Home');
var Battle = require('./Battle');
var Results = require('./Results');

// Component's properties:
// state
// lifecycle event
// UI (render)

class App extends React.Component {
	render() {
		return (
      <Router>
        <div className='container'>
          <Nav />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/battle' component={Battle} />
            <Route exact path='/battle/results' component={Results} />
            <Route path='/popular' component={Popular} />
            <Route render={ function () {
                return <p>Not found</p>
            }} />
          </Switch>
        </div>
      </Router>
		)
	}
}

module.exports = App;

class Users extends React.Component {
  render() {
    return (
      <ul>
        {this.props.list.map(function (friend) {
          return <li>{friend}</li>
        })}
      </ul>
    )
  }
}
Users.propTypes = {
  list: PropTypes.array.isRequired
}